# General
alias ls='ls -al'

# CTAGS
alias ctags-drupal='ctags --langmap=php:.engine.inc.module.theme.install.php --php-kinds=cdfi --languages=php --recurse'
alias ctags-php='phpctags --exclude='vendor' --append=yes --recurse=yes .'

# DRUPAL
alias tests-clean='php scripts/run-tests.sh --clean'
alias tests-run='php scripts/run-tests.sh --verbose --color'

# #GIT
# BFG repo cleaner
alias bfg='java -jar ~/bfg.jar'
# Create a new branch off of master
alias gcb='git checkout master; git pull; git checkout -b'
# Go to branch on Github web
alias gtb='chmod 755 ~/goto-github-branch.sh; . ~/goto-github-branch.sh'
# Status
alias gs='git status'

# GZIP
alias gzip='tar -cvzf'
alias ungzip='tar -xvf'

# TOGGL
alias sw='php ~/Sites/toggl-tempo/toggl-tempo/index.php'
alias sw-yesterday='sw --startdate=yesterday --enddate=yesterday'
