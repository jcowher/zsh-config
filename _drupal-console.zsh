# DRUPAL CONSOLE
if ! which drupal >/dev/null; then
  curl https://drupalconsole.com/installer -L -o drupal.phar && {
    mv drupal.phar /usr/local/bin/drupal
    chmod +x /usr/local/bin/drupal
    drupal init
  }
fi
