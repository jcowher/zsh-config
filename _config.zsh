# When using tab completion, ensure that paths are case sensitive;
CASE_SENSITIVE="true"

# Disable marking untracked files under VCS as dirty. This makes
# repository status check for larger repositories much, much faster.
DISABLE_UNTRACKED_FILES_DIRTY="true"
