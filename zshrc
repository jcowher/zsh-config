source ~/.zsh/_config.zsh
source ~/.zsh/_antigen.zsh
source ~/.zsh/_autocomplete.zsh
source ~/.zsh/_drupal-console.zsh
source ~/.zsh/_aliases.zsh

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
