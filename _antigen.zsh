# ANTIGEN - plugin manager
source ~/.zsh/antigen.zsh

# LOAD LIBRARY
antigen use oh-my-zsh

# BUNDLES
antigen bundles <<EOBUNDLES

  # Guess what to install when running an unknown command
  command-not-found

  # Helper for extracting different types of archives
  extract

  # Help working with GIT
  git
  git-extras
  github

  # Colors
  colored-man-pages
  colorize

  # Mac specific
  brew
  osx

  # Syntax highlighting
  zsh-users/zsh-syntax-highlighting

  # Auto Completions
  zsh-users/zsh-completions

EOBUNDLES

# APPLY
antigen apply

# LOAD OH-MY-ZSH
source $ZSH/oh-my-zsh.sh

# THEME
antigen theme agnoster
